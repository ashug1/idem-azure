State Examples
==============

Let's try some more examples.

Resource Group Create Example
+++++++++++++++++++++++++++++

Here's a simple example of creating a Resource group on Azure.

You can add any tags you want to the list under **tags**.

.. code-block:: yaml

    create_resource_group:
      azure.resource_management.resource_groups.present:
        - name: test-rg
        - resource_group_name: test-rg
        - location: eastus
        - tags:
            Name: test-idem-cloud-demo
            owner: Tom

Create a text file with the above contents at a location such as
`~/rg_test.sls`. Then we can use that file to create our Resource Group as we've
described.

.. code-block:: bash

    idem state ~/rg_test.sls

Output

.. code-block::

    --------
          ID: create_resource_group
    Function: azure.resource_management.resource_groups.present
      Result: True
     Comment: ("Created azure.resource_management.resource_groups 'test-rg'", "azure.resource_management.resource_groups 'test-rg' has no property need to be updated.")
     Changes:
    new:
        ----------
        name:
            test-rg
        resource_id:
            /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg
        resource_group_name:
            test-rg
        subscription_id:
            b8e40357-9bb6-4c41-9b43-a9ba0fd08160
        location:
            eastus
        tags:
            ----------
            Name:
                test-idem-cloud-demo
            owner:
                Tom


    present: 1 created successfully

If you go look at your Azure console, you'll see that a new Resource Group named
`test-rg` has been created.


Remove Resource Group
+++++++++++++++++++++

If you want to delete that Resource Group with Idem, you can do it like this:


.. code-block:: bash

    idem state ~/rg_test.sls --invert

Output:

.. code-block::

    --------
          ID: create_resource_group
    Function: azure.resource_management.resource_groups.absent
      Result: True
     Comment: ("Deleted azure.resource_management.resource_groups 'test-rg'",)
     Changes:
    old:
        ----------
        name:
            test-rg
        resource_id:
            /subscriptions/b8e40357-9bb6-4c41-9b43-a9ba0fd08160/resourceGroups/test-rg
        resource_group_name:
            test-rg
        subscription_id:
            b8e40357-9bb6-4c41-9b43-a9ba0fd08160
        location:
            eastus
        tags:
            ----------
            Name:
                test-idem-cloud-demo
            owner:
                Tom


    absent: 1 deleted successfully

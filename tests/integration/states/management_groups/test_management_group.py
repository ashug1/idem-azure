import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_management_group(hub, ctx):
    """
    This test provisions a management group, describes management group, does a force update and deletes
     the provisioned management group.
    """
    # Create management group
    management_group_name = "test-management" + str(uuid.uuid4())
    mg_parameters = {
        "display_name": "test_management_group_1",
        "parent_id": "/providers/Microsoft.Management/managementGroups/4566e90f-143b-4cae-8d19-5b4aa1d1edd2",
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create management group with --test
    mg_ret = await hub.states.azure.management_groups.management_groups.present(
        test_ctx,
        name=management_group_name,
        management_group_name=management_group_name,
        **mg_parameters,
    )
    assert mg_ret["result"], mg_ret["comment"]
    assert not mg_ret["old_state"] and mg_ret["new_state"]
    assert (
        f"Would create azure.management_groups.management_groups '{management_group_name}'"
        in mg_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=mg_ret["new_state"],
        expected_old_state=None,
        expected_new_state=mg_parameters,
        management_group_name=management_group_name,
        idem_resource_name=management_group_name,
    )

    resource_id = mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_name}"
        == resource_id
    )

    # Create management group in real
    mg_ret = await hub.states.azure.management_groups.management_groups.present(
        ctx,
        name=management_group_name,
        management_group_name=management_group_name,
        **mg_parameters,
    )

    assert mg_ret["result"], mg_ret["comment"]
    assert not mg_ret["old_state"] and mg_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=mg_ret["new_state"],
        expected_old_state=None,
        expected_new_state=mg_parameters,
        management_group_name=management_group_name,
        idem_resource_name=management_group_name,
    )

    resource_id = mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2020-05-01",
        retry_count=60,
        retry_period=10,
        retry_policy=[403],
    )

    # TODO: This practice is not recommended, and we will come back and improve this test.
    await hub.tool.azure.resource.wait_for_describe(
        ctx,
        url=f"{ctx.acct.endpoint_url}/providers/Microsoft.Management/managementGroups?api-version=2020-05-01",
        management_group_name=management_group_name,
        retry_count=60,
        retry_period=10,
    )

    # Describe management group
    describe_ret = await hub.states.azure.management_groups.management_groups.describe(
        ctx
    )

    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.management_groups.management_groups.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        management_group_name=management_group_name,
        idem_resource_name=resource_id,
    )

    mg_update_parameters = {
        "display_name": "test_management_group_2",
        "parent_id": "/providers/Microsoft.Management/managementGroups/78587530-29a0-4cb3-9080-4a3296cb3fdf",
    }
    # Update management group with --test
    mg_ret = await hub.states.azure.management_groups.management_groups.present(
        test_ctx,
        name=management_group_name,
        management_group_name=management_group_name,
        **mg_update_parameters,
    )
    assert mg_ret["result"], mg_ret["comment"]
    assert mg_ret["old_state"] and mg_ret["new_state"]
    assert (
        f"Would update azure.management_groups.management_groups '{management_group_name}'"
        in mg_ret["comment"]
    )
    check_returned_states(
        old_state=mg_ret["old_state"],
        new_state=mg_ret["new_state"],
        expected_old_state=mg_parameters,
        expected_new_state=mg_update_parameters,
        management_group_name=management_group_name,
        idem_resource_name=management_group_name,
    )
    resource_id = mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_name}"
        == resource_id
    )

    # Update management group in real
    mg_ret = await hub.states.azure.management_groups.management_groups.present(
        ctx,
        name=management_group_name,
        management_group_name=management_group_name,
        **mg_update_parameters,
    )
    assert mg_ret["result"], mg_ret["comment"]
    assert mg_ret["old_state"] and mg_ret["new_state"]
    assert (
        f"Updated azure.management_groups.management_groups '{management_group_name}'"
        in mg_ret["comment"]
    )
    check_returned_states(
        old_state=mg_ret["old_state"],
        new_state=mg_ret["new_state"],
        expected_old_state=mg_parameters,
        expected_new_state=mg_update_parameters,
        management_group_name=management_group_name,
        idem_resource_name=management_group_name,
    )
    resource_id = mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2020-05-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete management group with --test
    mg_del_ret = await hub.states.azure.management_groups.management_groups.absent(
        test_ctx,
        name=management_group_name,
        management_group_name=management_group_name,
    )
    assert mg_del_ret["result"], mg_del_ret["comment"]
    assert mg_del_ret["old_state"] and not mg_del_ret["new_state"]
    assert (
        f"Would delete azure.management_groups.management_groups '{management_group_name}'"
        in mg_del_ret["comment"]
    )
    check_returned_states(
        old_state=mg_del_ret["old_state"],
        new_state=None,
        expected_old_state=mg_update_parameters,
        expected_new_state=None,
        management_group_name=management_group_name,
        idem_resource_name=management_group_name,
    )

    # Delete management group in real
    mg_del_ret = await hub.states.azure.management_groups.management_groups.absent(
        ctx,
        name=management_group_name,
        management_group_name=management_group_name,
    )
    assert mg_del_ret["result"], mg_del_ret["comment"]
    assert mg_del_ret["old_state"] and not mg_del_ret["new_state"]
    assert (
        f"Deleted azure.management_groups.management_groups '{management_group_name}'"
        in mg_del_ret["comment"]
    )
    check_returned_states(
        old_state=mg_del_ret["old_state"],
        new_state=None,
        expected_old_state=mg_update_parameters,
        expected_new_state=None,
        management_group_name=management_group_name,
        idem_resource_name=management_group_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2020-05-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete management group again
    mg_del_ret = await hub.states.azure.management_groups.management_groups.absent(
        ctx,
        name=management_group_name,
        management_group_name=management_group_name,
    )
    assert mg_del_ret["result"], mg_del_ret["comment"]
    assert not mg_del_ret["old_state"] and not mg_del_ret["new_state"]
    assert (
        f"azure.management_groups.management_groups '{management_group_name}' already absent"
        in mg_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    management_group_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert management_group_name == old_state.get("management_group_name")
        if old_state.get("display_name") is not None:
            assert expected_old_state["display_name"] == old_state.get("display_name")
        if old_state.get("parent_id") is not None:
            assert expected_old_state["parent_id"] == old_state.get("parent_id")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert management_group_name == new_state.get("management_group_name")
        if new_state.get("display_name") is not None:
            assert expected_new_state["display_name"] == new_state.get("display_name")
        if new_state.get("parent_id") is not None:
            assert expected_new_state["parent_id"] == new_state.get("parent_id")

import copy
from collections import ChainMap

import dict_tools.differ as differ
import pytest

RESOURCE_NAME = "my-resource"
name = "my-key-vault"
RESOURCE_GROUP_NAME = "my-resource-group"
VAULT_NAME = "my-key-vault"
location = "eastus"
tags = {"tag-key": "tag-value"}
SUBSCRIPTION_ID = "subscription_id"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "resource_group_name": RESOURCE_GROUP_NAME,
    "vault_name": VAULT_NAME,
    "subscription_id": SUBSCRIPTION_ID,
    "tags": {"tag-key": "tag-value"},
    "sku": {"family": "A", "name": "Premium"},
    "soft_delete_retention_days": 8,
    "enabled_for_deployment": "false",
    "enabled_for_disk_encryption": "true",
    "enabled_for_template_deployment": "true",
    "enable_rbac_authorization": "true",
    "public_network_access_enabled": "Enabled",
    "purge_protection_enabled": "true",
    "access_policies": [],
    "tenant_id": 00000000 - 0000 - 0000 - 0000 - 000000000000,
    "network_acls": {
        "bypass": "AzureServices",
        "default_action": "Deny",
        "ip_rules": [],
        "virtual_network_subnet_ids": [],
    },
}
RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "properties": {
        "sku": {"name": "Premium", "family": "A"},
        "tenantId": 0,
        "enabledForDeployment": "false",
        "enabledForDiskEncryption": "true",
        "enabledForTemplateDeployment": "true",
        "publicNetworkAccess": "Enabled",
        "enableRbacAuthorization": "true",
        "softDeleteRetentionInDays": 8,
        "enablePurgeProtection": "true",
        "accessPolicies": [],
        "networkAcls": {
            "bypass": "AzureServices",
            "defaultAction": "Deny",
            "ipRules": [],
            "virtualNetworkRules": [],
        },
    },
    "tags": {"tag-key": "tag-value"},
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of key vault. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.key_vault.vault.present = (
        hub.states.azure.key_vault.vault.present
    )
    mock_hub.tool.azure.key_vault.vault.convert_key_vault_to_present = (
        hub.tool.azure.key_vault.vault.convert_key_vault_to_present
    )
    mock_hub.tool.azure.key_vault.vault.convert_present_to_key_vault = (
        hub.tool.azure.key_vault.vault.convert_present_to_key_vault
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.key_vault.vault.get = hub.exec.azure.key_vault.vault.get

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.KeyVault/vaults/{VAULT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert VAULT_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert VAULT_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.key_vault.vault.present(
        test_ctx, RESOURCE_NAME, **RESOURCE_PARAMETERS
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Would create azure.key_vault.vault '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    # Test present() with --test flag off
    ret = await mock_hub.states.azure.key_vault.vault.present(
        ctx, RESOURCE_NAME, **RESOURCE_PARAMETERS
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Created azure.key_vault.vault '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of key vault. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.key_vault.vault.present = (
        hub.states.azure.key_vault.vault.present
    )
    mock_hub.tool.azure.key_vault.vault.convert_key_vault_to_present = (
        hub.tool.azure.key_vault.vault.convert_key_vault_to_present
    )
    mock_hub.tool.azure.key_vault.vault.convert_present_to_key_vault = (
        hub.tool.azure.key_vault.vault.convert_present_to_key_vault
    )
    mock_hub.tool.azure.key_vault.vault.update_key_vault_payload = (
        hub.tool.azure.key_vault.vault.update_key_vault_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.key_vault.vault.get = hub.exec.azure.key_vault.vault.get

    resource_parameters_update = {
        "location": "eastus",
        "resource_group_name": RESOURCE_GROUP_NAME,
        "vault_name": VAULT_NAME,
        "subscription_id": SUBSCRIPTION_ID,
        "tags": {"tag-key": "tag-value"},
    }

    resource_parameters_update_raw = {
        "location": "eastus",
        "properties": {},
        "tags": {"tag-new-key": "tag-new-key"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.KeyVault/vaults/{VAULT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.KeyVault/vaults/{VAULT_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert VAULT_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.key_vault.vault.present(
        test_ctx,
        RESOURCE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"azure.key_vault.vault '{RESOURCE_NAME}' doesn't need to be updated."
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.key_vault.vault.present(
        ctx,
        RESOURCE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of key vault. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.key_vault.vault.absent = (
        hub.states.azure.key_vault.vault.absent
    )
    mock_hub.tool.azure.key_vault.vault.convert_key_vault_to_present = (
        hub.tool.azure.key_vault.vault.convert_key_vault_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.key_vault.vault.get = hub.exec.azure.key_vault.vault.get

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert VAULT_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.key_vault.vault.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VAULT_NAME, SUBSCRIPTION_ID
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert f"azure.key_vault.vault '{VAULT_NAME}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of key vault. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.key_vault.vault.absent = (
        hub.states.azure.key_vault.vault.absent
    )
    mock_hub.tool.azure.key_vault.vault.convert_key_vault_to_present = (
        hub.tool.azure.key_vault.vault.convert_key_vault_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.key_vault.vault.get = hub.exec.azure.key_vault.vault.get

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.KeyVault/vaults/{VAULT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert VAULT_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.key_vault.vault.absent(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VAULT_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Would delete azure.key_vault.vault '{VAULT_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.key_vault.vault.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VAULT_NAME, SUBSCRIPTION_ID
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.key_vault.vault '{VAULT_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of key vault.
    """
    mock_hub.states.azure.key_vault.vault.describe = (
        hub.states.azure.key_vault.vault.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.key_vault.vault.convert_key_vault_to_present = (
        hub.tool.azure.key_vault.vault.convert_key_vault_to_present
    )
    mock_hub.exec.azure.key_vault.vault.list = hub.exec.azure.key_vault.vault.list

    resource_id = (
        f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.KeyVault/vaults/{VAULT_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {
                    "id": resource_id,
                    "name": RESOURCE_GROUP_NAME,
                    **RESOURCE_PARAMETERS_RAW,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.key_vault.vault.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.key_vault.vault.present" in ret_value.keys()
    described_resource = ret_value.get("azure.key_vault.vault.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert VAULT_NAME == old_state.get("vault_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert VAULT_NAME == new_state.get("vault_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")

import copy
from collections import ChainMap

import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
ROUTE_TABLE_NAME = "my-route-table"
ROUTE_NAME = "my-route"
SUBSCRIPTION_ID = "subscription-id"
RESOURCE_PARAMETERS = {
    "address_prefix": "10.0.0.0/26",
    "next_hop_type": "VirtualAppliance",
    "next_hop_ip_address": "10.1.0.0",
    "subscription_id": SUBSCRIPTION_ID,
}
RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "addressPrefix": "10.0.0.0/26",
        "nextHopType": "VirtualAppliance",
        "nextHopIpAddress": "10.1.0.0",
    }
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of route table. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.network.routes.present = (
        hub.states.azure.network.routes.present
    )
    mock_hub.tool.azure.network.routes.convert_raw_routes_to_present = (
        hub.tool.azure.network.routes.convert_raw_routes_to_present
    )
    mock_hub.tool.azure.network.routes.convert_present_to_raw_routes = (
        hub.tool.azure.network.routes.convert_present_to_raw_routes
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.routes.get = hub.exec.azure.network.routes.get

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.routes.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Would create azure.network.routes '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.routes.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Created azure.network.routes '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of route table. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.network.routes.present = (
        hub.states.azure.network.routes.present
    )
    mock_hub.tool.azure.network.routes.convert_raw_routes_to_present = (
        hub.tool.azure.network.routes.convert_raw_routes_to_present
    )
    mock_hub.tool.azure.network.routes.convert_present_to_raw_routes = (
        hub.tool.azure.network.routes.convert_present_to_raw_routes
    )
    mock_hub.tool.azure.network.routes.update_route_payload = (
        hub.tool.azure.network.routes.update_route_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.routes.get = hub.exec.azure.network.routes.get
    resource_parameters_update = {
        "address_prefix": "10.0.2.0/24",
        "next_hop_type": "VirtualAppliance",
        "next_hop_ip_address": "10.0.1.0",
        "subscription_id": SUBSCRIPTION_ID,
    }
    resource_parameters_update_raw = {
        "properties": {
            "addressPrefix": "10.0.2.0/24",
            "nextHopType": "VirtualAppliance",
            "nextHopIpAddress": "10.0.1.0",
        }
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.routes.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Would update azure.network.routes '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.routes.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Updated azure.network.routes '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of route table. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.network.routes.absent = hub.states.azure.network.routes.absent
    mock_hub.tool.azure.network.routes.convert_raw_routes_to_present = (
        hub.tool.azure.network.routes.convert_raw_routes_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.routes.get = hub.exec.azure.network.routes.get
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_TABLE_NAME in url
        assert ROUTE_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.network.routes.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert f"azure.network.routes '{RESOURCE_NAME}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of route table. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.network.routes.absent = hub.states.azure.network.routes.absent
    mock_hub.tool.azure.network.routes.convert_raw_routes_to_present = (
        hub.tool.azure.network.routes.convert_raw_routes_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.routes.get = hub.exec.azure.network.routes.get
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert RESOURCE_GROUP_NAME in url
        assert ROUTE_NAME in url
        assert ROUTE_TABLE_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.routes.absent(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Would delete azure.network.routes '{RESOURCE_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.network.routes.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        ROUTE_TABLE_NAME,
        ROUTE_NAME,
        SUBSCRIPTION_ID,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.network.routes '{RESOURCE_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of route table.
    """
    mock_hub.states.azure.network.routes.describe = (
        hub.states.azure.network.routes.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.network.routes.convert_raw_routes_to_present = (
        hub.tool.azure.network.routes.convert_raw_routes_to_present
    )
    mock_hub.exec.azure.network.routes.list = hub.exec.azure.network.routes.list

    RESOURCE_PARAMETERS_RAW_ROUTE_TABLES = {
        "location": "eastus",
        "tags": {"new-tag-key": "new-tag-value"},
        "properties": {
            "disableBgpRoutePropagation": False,
            "routes": [
                {
                    "name": "test-route",
                    "id": f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}",
                    **RESOURCE_PARAMETERS_RAW,
                }
            ],
        },
    }
    resource_id = (
        f"/subscriptions/{SUBSCRIPTION_ID}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/routeTables/{ROUTE_TABLE_NAME}/routes/{ROUTE_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {
                    "id": resource_id,
                    "name": RESOURCE_NAME,
                    **RESOURCE_PARAMETERS_RAW_ROUTE_TABLES,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.routes.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.network.routes.present" in ret_value.keys()
    described_resource = ret_value.get("azure.network.routes.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert ROUTE_TABLE_NAME == old_state.get("route_table_name")
        assert ROUTE_NAME == old_state.get("route_name")
        assert expected_old_state["address_prefix"] == old_state.get("address_prefix")
        assert expected_old_state["next_hop_type"] == old_state.get("next_hop_type")
        assert expected_old_state["next_hop_ip_address"] == old_state.get(
            "next_hop_ip_address"
        )
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert ROUTE_TABLE_NAME == new_state.get("route_table_name")
        assert ROUTE_NAME == new_state.get("route_name")
        assert expected_new_state["address_prefix"] == new_state.get("address_prefix")
        assert expected_new_state["next_hop_type"] == new_state.get("next_hop_type")
        assert expected_new_state["next_hop_ip_address"] == new_state.get(
            "next_hop_ip_address"
        )
        assert expected_new_state["subscription_id"] == new_state.get("subscription_id")

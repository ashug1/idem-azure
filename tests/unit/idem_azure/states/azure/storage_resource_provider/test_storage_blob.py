import copy
from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
ACCOUNT_NAME = "my-storage-account"
CONTAINER_NAME = "testblob06"
RESOURCE_PARAMETERS = {
    "subscription_id": "12345678-1234-1234-1234-aaabc1234aaa",
    "account_name": "my-storage-account",
    "resource_group_name": "my-resource-group",
    "public_access": "Blob",
    "type": "Microsoft.Storage/storageAccounts/blobServices/containers",
    "etag": "dummy_tag",
    "metadata": {"testKey": "testValue"},
    "remaining_retention_days": 0,
}

RESOURCE_PARAMETERS_RAW = {
    "id": "/subscriptions/12345678-1234-1234-1234-aaabc1234aaa/resourceGroups/my-resource-group/providers/Microsoft.Storage/storageAccounts/my-storage-account/blobServices/default/containers/testblob06",
    "name": "my-resource",
    "container_name": "testblob06",
    "type": "Microsoft.Storage/storageAccounts/blobServices/containers",
    "properties": {
        "metadata": {"testKey": "testValue"},
        "deleted": False,
        "remainingRetentionDays": 0,
        "publicAccess": "Blob",
        "hasImmutabilityPolicy": False,
        "hasLegalHold": False,
    },
}

ACCOUNTS_RESOURCE_PARAMETERS_RAW = {
    "sku": {
        "name": "Standard_LRS",
        "tier": "Standard",
    },
    "kind": "BlockBlobStorage",
    "location": "eastus",
    "properties": {
        "isHnsEnabled": "true",
        "isNfsV3Enabled": "true",
        "supportsHttpsTrafficOnly": "false",
        "minimumTlsVersion": "TLS1_2",
        "allowBlobPublicAccess": "true",
        "allowSharedKeyAccess": "true",
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of storage blob. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.storage_resource_provider.storage_blob.present = (
        hub.states.azure.storage_resource_provider.storage_blob.present
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present
    )
    mock_hub.exec.azure.storage_resource_provider.storage_blob.get = (
        hub.exec.azure.storage_resource_provider.storage_blob.get
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_present_to_raw_storage_blob = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_present_to_raw_storage_blob
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.update_storage_blob_payload = (
        hub.tool.azure.storage_resource_provider.storage_blob.update_storage_blob_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}/blobServices/default/containers/{CONTAINER_NAME}",
            "name": RESOURCE_NAME,
            "resource_group_name": RESOURCE_GROUP_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.storage_resource_provider.blob",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert CONTAINER_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert json == RESOURCE_PARAMETERS_RAW
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    present_params_for_test = RESOURCE_PARAMETERS
    present_params_for_test.pop("type")
    present_params_for_test.pop("etag")
    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.present(
        ctx=test_ctx,
        name=RESOURCE_NAME,
        container_name=CONTAINER_NAME,
        **present_params_for_test,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.storage_resource_provider.blob '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=present_params_for_test,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.present(
        ctx=ctx,
        name=RESOURCE_NAME,
        container_name=CONTAINER_NAME,
        **present_params_for_test,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.storage_resource_provider.storage_blob '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=present_params_for_test,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of storage blob. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.states.azure.storage_resource_provider.storage_blob.present = (
        hub.states.azure.storage_resource_provider.storage_blob.present
    )
    mock_hub.exec.azure.storage_resource_provider.storage_blob.get = (
        hub.exec.azure.storage_resource_provider.storage_blob.get
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_raw = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_raw
    )

    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_present_to_raw_storage_blob = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_present_to_raw_storage_blob
    )
    # Check if we need to mock it......
    mock_hub.tool.azure.storage_resource_provider.storage_blob.update_storage_blob_payload = (
        hub.tool.azure.storage_resource_provider.storage_blob.update_storage_blob_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    metadata_dict = {"testKeyChanged": "testValueChanged"}

    resource_parameters_update = {
        "has_immutability_policy": True,
        "metadata": metadata_dict,
        "remaining_retention_days": 100,
    }

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}/blobServices/default/containers/{CONTAINER_NAME}"
    )
    expected_get = {
        "ret": {
            "id": resource_id,
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}/blobServices/default/containers/{CONTAINER_NAME}",
            "name": RESOURCE_NAME,
            "resource_group_name": RESOURCE_GROUP_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert CONTAINER_NAME in url
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present.return_value = {
        "subscription_id": "12345678-1234-1234-1234-aaabc1234aaa",
        "resource_group_name": RESOURCE_GROUP_NAME,
        "account_name": ACCOUNT_NAME,
        "name": RESOURCE_NAME,
        "type": "Microsoft.Storage/storageAccounts/blobServices/containers",
        "resource_id": "/subscriptions/12345678-1234-1234-1234-aaabc1234aaa/resourceGroups/my-resource-group/providers/Microsoft.Storage/storageAccounts/my-storage-account/blobServices/default/containers/testblob06",
        "etag": "dummy_tag",
        "properties": {
            "has_immutability_policy": False,
            "metadata": {"testKey": "testValue"},
            "public_access": "Blob",
        },
    }

    mock_hub.tool.azure.uri.get_parameter_value_in_dict.return_value = {
        "subscription_id": "12345678-1234-1234-1234-aaabc1234aaa",
        "resource_group_name": RESOURCE_GROUP_NAME,
        "account_name": ACCOUNT_NAME,
    }

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.present(
        ctx=test_ctx,
        name=RESOURCE_NAME,
        container_name=CONTAINER_NAME,
        account_name=ACCOUNT_NAME,
        resource_group_name=RESOURCE_GROUP_NAME,
        subscription_id="12345678-1234-1234-1234-aaabc1234aaa",
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    # Need to revisit
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.storage_resource_provider.storage_blob '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.present(
        ctx=ctx,
        name=RESOURCE_NAME,
        container_name=CONTAINER_NAME,
        account_name=ACCOUNT_NAME,
        resource_group_name=RESOURCE_GROUP_NAME,
        subscription_id="12345678-1234-1234-1234-aaabc1234aaa",
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of storage blob.
    When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.storage_resource_provider.storage_blob.absent = (
        hub.states.azure.storage_resource_provider.storage_blob.absent
    )
    mock_hub.exec.azure.storage_resource_provider.storage_blob.get = (
        hub.exec.azure.storage_resource_provider.storage_blob.get
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        assert CONTAINER_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.absent(
        ctx,
        name=RESOURCE_NAME,
        subscription_id=ctx.acct.subscription_id,
        resource_group_name=RESOURCE_GROUP_NAME,
        account_name=ACCOUNT_NAME,
        container_name=CONTAINER_NAME,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.storage_resource_provider.storage_blob '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of storage blob. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.storage_resource_provider.storage_blob.absent = (
        hub.states.azure.storage_resource_provider.storage_blob.absent
    )
    mock_hub.exec.azure.storage_resource_provider.storage_blob.get = (
        hub.exec.azure.storage_resource_provider.storage_blob.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}/blobServices/default/containers/{CONTAINER_NAME}"
    )
    expected_get = {
        "ret": {
            "id": resource_id,
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert ACCOUNT_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.absent(
        test_ctx,
        name=RESOURCE_NAME,
        subscription_id=ctx.acct.subscription_id,
        resource_group_name=RESOURCE_GROUP_NAME,
        account_name=ACCOUNT_NAME,
        container_name=CONTAINER_NAME,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.storage_resource_provider.storage_blob '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.absent(
        ctx,
        name=RESOURCE_NAME,
        subscription_id=ctx.acct.subscription_id,
        resource_group_name=RESOURCE_GROUP_NAME,
        account_name=ACCOUNT_NAME,
        container_name=CONTAINER_NAME,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.storage_resource_provider.storage_blob '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of storage blob.
    """
    mock_hub.states.azure.storage_resource_provider.storage_blob.describe = (
        hub.states.azure.storage_resource_provider.storage_blob.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    mock_hub.exec.azure.storage_resource_provider.storage_accounts.list = (
        hub.exec.azure.storage_resource_provider.storage_accounts.list
    )
    mock_hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present = (
        hub.tool.azure.storage_resource_provider.storage_accounts.convert_raw_storage_accounts_to_present
    )

    resource_id_accounts = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}"
    )
    expected_list_accounts = {
        "ret": {
            "value": [
                {
                    "id": resource_id_accounts,
                    "name": RESOURCE_NAME,
                    **ACCOUNTS_RESOURCE_PARAMETERS_RAW,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    mock_hub.exec.azure.storage_resource_provider.storage_blob.list = (
        hub.exec.azure.storage_resource_provider.storage_blob.list
    )
    mock_hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present = (
        hub.tool.azure.storage_resource_provider.storage_blob.convert_raw_storage_blob_to_present
    )

    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Storage/storageAccounts/{ACCOUNT_NAME}/blobServices/default/containers/{CONTAINER_NAME}"
    )

    expected_list = {
        "ret": {
            "value": [
                {
                    "id": resource_id,
                    "name": RESOURCE_NAME,
                    "resource_group_name": RESOURCE_GROUP_NAME,
                    **RESOURCE_PARAMETERS_RAW,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.side_effect = [expected_list_accounts, expected_list]

    ret = await mock_hub.states.azure.storage_resource_provider.storage_blob.describe(
        ctx
    )

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.storage_resource_provider.storage_blob.present" in ret_value.keys()
    described_resource = ret_value.get(
        "azure.storage_resource_provider.storage_blob.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("resource_id")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert ACCOUNT_NAME == old_state.get("account_name")
        assert expected_old_state["subscription_id"] == old_state.get("subscription_id")
        if expected_old_state.get("type"):
            assert expected_old_state["type"] == old_state.get("type")
        if expected_old_state.get("etag"):
            assert expected_old_state["etag"] == old_state.get("etag")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert ACCOUNT_NAME == new_state.get("account_name")
        if expected_new_state.get("subscription_id"):
            assert expected_new_state["subscription_id"] == new_state.get(
                "subscription_id"
            )
        if expected_new_state.get("type"):
            assert expected_new_state["type"] == new_state.get("type")
        if expected_new_state.get("etag"):
            assert expected_new_state["etag"] == new_state.get("etag")

import pytest

profiles = {
    "azure.mock": {
        "default": {
            "client_id": "clientID",
            "secret": "secret",
            "subscription_id": "subscriptionId",
            "endpoint_url": "https://mock-azure.com/",
            "tenant": "tenantId",
        },
        "test_development_idem_azure": {
            "client_id": "test-clientID",
            "secret": "test-secret",
            "subscription_id": "test-subscriptionId",
            "endpoint_url": "https://mock-azure.com/",
            "tenant": "test-tenantId",
        },
    }
}


@pytest.mark.asyncio
async def test_gather(hub):
    res = await hub.acct.azure.mock.gather(profiles)
    assert "default" and "test_development_idem_azure" in res
    assert profiles["azure.mock"]["default"] == res["default"]
    assert (
        profiles["azure.mock"]["test_development_idem_azure"]
        == res["test_development_idem_azure"]
    )
